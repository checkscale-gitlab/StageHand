#!/bin/sh

set -e

echo " === Test ==="

docker-compose -f docker-compose.ci.yml \
  run \
  --rm \
  app \
  mix test
