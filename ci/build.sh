#!/bin/sh

set -e

echo " === Build ==="

docker build \
  --tag ${IMAGE_LATEST} \
  --tag ${IMAGE_VERSIONED} \
  --file ./app/Dockerfile.dev \
  ./app

docker push ${IMAGE_LATEST}
docker push ${IMAGE_VERSIONED}
