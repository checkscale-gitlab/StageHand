.PHONY: app-sh encrypt-secrets help

APP_NAME ?= `grep 'app:' app/mix.exs | sed -e 's/\[//g' -e 's/ //g' -e 's/app://' -e 's/[:,]//g'`
APP_VSN ?= `grep 'version:' app/mix.exs | cut -d '"' -f2`
DOCKER_REPO ?= rodeoclash
DOCKER_IMAGE ?= $(DOCKER_REPO)/$(APP_NAME)
DOCKER_IMAGE_VERSIONED ?= $(DOCKER_IMAGE):$(APP_VSN)
DOCKER_IMAGE_LATEST ?= $(DOCKER_IMAGE):latest
TERRAFORM_VERSION ?= 0.12.29

# Show details about the current version
help:
	@echo "$(APP_NAME):$(APP_VSN)"
	@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Build the production docker image
build:
	docker build \
			--file app/Dockerfile.prod \
			--build-arg APP_NAME=$(APP_NAME) \
			--build-arg APP_VSN=$(APP_VSN) \
			--tag $(DOCKER_IMAGE_VERSIONED) \
			--tag $(DOCKER_IMAGE_LATEST) \
			--target=production \
			./app

dockerhub_login:
	docker login -u rodeoclash -p $(DOCKERHUB_PASSWORD)

# Releases Stagehand
dockerhub_push: build dockerhub_login
	docker push $(DOCKER_IMAGE_VERSIONED)
	docker push $(DOCKER_IMAGE_LATEST)

# Releases Stagehand by the image to Dockerhub
release: dockerhub_login dockerhub_push

# Fetch the terraform binary, required to load into Ansible container
fetch_terraform:
	wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
	unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Deploys the application by running the deploy playbook
deploy: release fetch_terraform
	docker \
		run \
		--env ANSIBLE_VAULT_PASSWORD_FILE=/ansible_vault_password \
		--env DEPLOY_IMAGE=$(DOCKER_IMAGE_VERSIONED) \
		--env RELEASE=$(CI_COMMIT_SHA) \
		--rm \
		--volume $(CURDIR)/terraform:/usr/local/bin/terraform \
		--volume $(CURDIR):/repo \
		--volume $(ANSIBLE_VAULT_PASSWORD_FILE):/ansible_vault_password \
		--workdir /repo/infra/ansible \
		willhallonline/ansible:2.8-alpine-3.8  \
			ansible-playbook deploy.yml -v

# Shell into the app
app-sh:
	docker-compose run --rm app bash

app-sh-root:
	docker-compose run --user=root:root --rm app bash

# Encrypt app prod secret into Ansible
encrypt-secrets:
	ansible-vault encrypt ./app/config/kubeconfig.prod --output - > ./infra/ansible/support/kubeconfig.prod.enc
