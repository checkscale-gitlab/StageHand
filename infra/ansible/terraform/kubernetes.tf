resource "digitalocean_kubernetes_cluster" "app" {
  lifecycle {
    prevent_destroy = true
  }

  name    = "stagehand"
  region  = var.region
  version = "1.16.6-do.2"

  node_pool {
    name       = "worker-pool"
    size       = "s-1vcpu-2gb"
    node_count = 2
  }
}

