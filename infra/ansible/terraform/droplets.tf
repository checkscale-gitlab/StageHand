resource "digitalocean_droplet" "app" {
  lifecycle {
    prevent_destroy = true
  }

  image    = "ubuntu-18-04-x64"
  name     = "stagehand"
  region   = var.region
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  ipv6     = "true"
}

output "app_ip" {
  value = digitalocean_droplet.app.ipv4_address
}

