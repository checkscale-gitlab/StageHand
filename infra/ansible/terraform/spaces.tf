resource "digitalocean_spaces_bucket" "academy_website_assets" {
  name   = "academy-website-assets"
  region = var.region

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT", "POST", "DELETE"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
}
