# [StageHand](https://stagehand.dev/)

[![pipeline status](https://gitlab.com/Rodeoclash/StageHand/badges/master/pipeline.svg)](https://gitlab.com/Rodeoclash/StageHand/commits/master)

Stagehand is a tool for easily creating review apps from your codebase. Review apps are instances of your code, usually based on a commit, that are easy to spin up and view.

# How does it work?

Stagehand works by providing an on-demand, isolated environments for your previews within a Kubernetes cluster. Stagehand is designed to work with Dockerized envrionments. For example, if you use Docker Compose to orchastrate your local development environment with a database, backend and frontend services, it is simple to translate this to work with Stagehand.

Currently Stagehand works with Bitbucket and GitHub.

# Why review apps?

Review apps provide a number of benefits to the code development workflow. They allow:

- Previews features to product owners and other stakeholders without needing to deploy to your staging branch.
- Testing the behaviour of code alongside reviewing it.
- Viewing the visual effects and design elements of a pull request which are not obvious in a diff.

# Screenshots

![Screenshot_from_2019-10-30_20-23-36](/uploads/dcc1f853bd11122d9a0fa3f7692e70d8/Screenshot_from_2019-10-30_20-23-36.png)
![Screenshot_from_2019-10-30_20-31-03](/uploads/7a8c97e575e01ee39f91124653e7f62c/Screenshot_from_2019-10-30_20-31-03.png)
![Screenshot_from_2019-10-30_20-40-00](/uploads/a950274eb3fea8d8bc618757d0404680/Screenshot_from_2019-10-30_20-40-00.png)

# Installation

**Installation instructions are a work in progress!**

The use of Stagehand is predicated on the availability of a Kubernetes cluster to run it in. While it is likely possible that Stagehand can be hosted within the Kubernetes cluster, it has only been tested when hosted externally.

1. Provision a machine to run Stagehand on. You will need to access this machine from a web browser to complete the initial configuration. After configuration has been completed you can disable access.
2. Provision a Postgres 11.x database for Stagehand to use.
3. Configure Stagehand using environment variables, see the file `infra/ansible/deploy.yml` for the possible values that you can use here.
4. Configure your Kubernetes environment. See / run the file `infra/k8s/install.sh`. This will allow you to construct a `kubeconfig.prod` which must be mounted into the container to allow Kubernetes to connect to the Kubernetes API.
5. Start the Stagehand application. Ensure the log output does not contain any Kubernetes API connection errors.
6. Visit the Stagehand application in a browser and configure a connection to Github / Bitbucket.

# Usage

Stagehand is designed to be used with a minimum of configuration from within the application. Instead, configuration of the review application is done when creating the preview. This allows for very dynamic configuration of the preview.

To get started, see a small example: https://bitbucket.org/Rodeoclash/stagehandtest

This example contains the smallest possible example of using Stagehand. Of particular note are the files

- _bitbucket-pipelines.yml_ This configures the bitbucket pipeline to execute a step to publish a preview.
- _docker-compose.yml_ This is executed inside the preview environment.
- _stagehand.json.mo_ This configures the preview in Stagehand. It is in a `mustache` template format which is converted to JSON before sending to Stagehand.
- _stagehand_startup.sh_ This file is executed when the preview is started. It pulls the required images and starts `docker-compose`.
- _bitbucket-pipelines/deploy_preview.sh_ This file is executed in Bitbucket pipelines and activates the preview.

After creation of a preview from the pipelines (or Github actions step) a link will be created in the PR that when clicked, will launch the preview.
