defmodule App.Repo.Migrations.CreateProviderAuthorisations do
  use Ecto.Migration

  def change do
    create table(:provider_authorisations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :expires, :boolean, null: false
      add :expires_at, :integer
      add :provider_id, references(:providers, on_delete: :delete_all, type: :binary_id), null: false
      add :refresh_token, :string
      add :token, :string, null: false
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false
      timestamps()
    end

    create unique_index(:provider_authorisations, [:provider_id, :user_id])
  end
end
