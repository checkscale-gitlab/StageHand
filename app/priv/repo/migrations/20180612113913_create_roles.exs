defmodule App.Repo.Migrations.CreateRoles  do
  use Ecto.Migration

  def change do
    create table(:roles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :organisation_id, references(:organisations, on_delete: :delete_all, type: :binary_id), null: false
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false
      timestamps()
    end

    create unique_index(:roles, [:organisation_id, :user_id])
  end
end
