defmodule App.Repo.Migrations.RenameOrgansiationsToProjects do
  use Ecto.Migration

  def change do
    # provider connection
    drop unique_index(:provider_connections, [:organisation_id, :provider_id, :user_id])
    rename table(:provider_connections), :organisation_id, to: :project_id
    create unique_index(:provider_connections, [:project_id, :provider_id, :user_id])

    # roles
    drop unique_index(:roles, [:organisation_id, :user_id])
    rename table(:roles), :organisation_id, to: :project_id
    create unique_index(:roles, [:project_id, :user_id])

    # previews
    rename table(:previews), :organisation_id, to: :project_id

    rename table("organisations"), to: table("projects")
  end
end
