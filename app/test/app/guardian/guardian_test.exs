defmodule App.GuardianTest do
  alias App.Guardian
  import App.Fixtures
  use App.DataCase, async: true

  test "subject_for_token/2" do
    user = insert(:user)
    assert Guardian.subject_for_token(user, nil) == {:ok, user.id}
  end

  test "resource_from_claims/1" do
    user = insert(:user)
    claims = %{"sub" => user.id}
    assert Guardian.resource_from_claims(claims) == {:ok, user}
  end
end
