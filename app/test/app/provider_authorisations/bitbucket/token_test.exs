defmodule App.ProviderAuthorisations.Bitbucket.TokenTest do
  alias App.{ProviderAuthorisations.Bitbucket.Token}
  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  @token "abcd5678"
  @expires_in 7200

  setup do
    Mox.verify_on_exit!()

    provider_authorisation = insert(:provider_authorisation)
    %{provider_authorisation: provider_authorisation}
  end

  describe "renew_if_expired/1" do
    test "when expired" do
      provider = insert(:provider, name: "Other")

      provider_authorisation =
        insert(:provider_authorisation, provider: provider, expires_at: 1_554_724_864)

      App.HTTPoisonMock
      |> expect(:post, fn _url, _body, _headers, _options ->
        {
          :ok,
          %HTTPoison.Response{
            body: """
              {
                "access_token": "1234",
                "expires_in": 7200
              }
            """
          }
        }
      end)

      assert {:ok, returned_provider_authorisation} =
               Token.renew_if_expired(provider_authorisation)

      assert returned_provider_authorisation.expires_at > provider_authorisation.expires_at
      assert returned_provider_authorisation.token == "1234"
    end

    test "when unexpired" do
      provider = insert(:provider, name: "Other")

      provider_authorisation =
        insert(:provider_authorisation, provider: provider, expires_at: 1_654_724_864)

      assert {:ok, provider_authorisation} == Token.renew_if_expired(provider_authorisation)
    end
  end

  test "renew/1", %{provider_authorisation: provider_authorisation} do
    config = Application.get_env(:ueberauth, Ueberauth.Strategy.Bitbucket.OAuth)

    response = {
      :ok,
      %HTTPoison.Response{
        body: """
          {
            "access_token": "#{@token}",
            "expires_in": #{@expires_in}
          }
        """
      }
    }

    App.HTTPoisonMock
    |> expect(:post, fn url, body, headers, options ->
      assert url == "https://bitbucket.org/site/oauth2/access_token"

      assert body == {
               :form,
               [
                 {"grant_type", "refresh_token"},
                 {"refresh_token", provider_authorisation.refresh_token}
               ]
             }

      assert headers == [
               {"Content-Type", "application/x-www-form-urlencoded"}
             ]

      assert options == [
               hackney: [
                 basic_auth: {
                   config[:client_id],
                   config[:client_secret]
                 }
               ]
             ]

      response
    end)

    current_time = DateTime.utc_now() |> DateTime.to_unix()

    assert {:ok, provider_authorisation} = Token.renew(provider_authorisation)
    assert provider_authorisation.token == @token
    assert provider_authorisation.expires_at >= current_time + @expires_in
  end
end
