defmodule App.Users.AuthorisationTest do
  alias App.{Users.Authorisation}
  import App.Fixtures
  use App.DataCase, async: true

  @auth %Ueberauth.Auth{
    credentials: %Ueberauth.Auth.Credentials{
      expires: true,
      expires_at: 1_568_121_091,
      refresh_token: "8bacf0b5ff71b030a9e51c3c41c7855b0336fc4c",
      token: "8bacf0b5ff71b030a9e51c3c41c7855b0336fc4c"
    },
    info: %Ueberauth.Auth.Info{
      email: "example@example.com"
    },
    provider: :github
  }

  setup do
    provider = insert(:provider, name: "github")
    user = insert(:user)
    %{provider: provider, user: user}
  end

  describe "find_or_create/2" do
    test "when does not exist", %{user: user, provider: provider} do
      assert {:ok, provider_authorisation} = Authorisation.find_or_create(@auth, user)

      assert provider_authorisation.provider_id == provider.id
      assert provider_authorisation.expires_at == @auth.credentials.expires_at
      assert provider_authorisation.refresh_token == @auth.credentials.refresh_token
      assert provider_authorisation.token == @auth.credentials.token
      assert provider_authorisation.user_id == user.id
    end

    test "when exists", %{user: user, provider: provider} do
      existing_provider_authorisation =
        insert(
          :provider_authorisation,
          user: user,
          token: "1234",
          provider: provider
        )

      assert {:ok, provider_authorisation} = Authorisation.find_or_create(@auth, user)
      assert provider_authorisation.id == existing_provider_authorisation.id
      assert provider_authorisation.provider_id == existing_provider_authorisation.provider_id
      assert provider_authorisation.user_id == existing_provider_authorisation.user_id
      assert provider_authorisation.expires_at == @auth.credentials.expires_at
      assert provider_authorisation.refresh_token == @auth.credentials.refresh_token
      assert provider_authorisation.token == @auth.credentials.token
    end
  end
end
