defmodule App.Previews.KubernetesV2Test do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews,
    Previews.KubernetesV2
  }

  import App.Fixtures
  import Mox

  use App.DataCase, async: false

  setup :verify_on_exit!
  setup :set_mox_global

  setup do
    Mox.verify_on_exit!()

    provider = insert(:provider)
    project = insert(:project)

    provider_authorisation = insert(:provider_authorisation, provider: provider)

    insert(:provider_connection,
      provider: provider,
      user: provider_authorisation.user,
      project: project
    )

    preview =
      insert(:preview, %{
        provider: provider,
        project: project
      })

    %{preview: preview}
  end

  test "start/1", %{preview: preview} do
    App.KubernetesV2Mock
    |> expect(
      :async,
      Expectation.read_all(preview, Response.read_all_missing(preview))
    )

    App.KubernetesV2Mock
    |> expect(:async, Expectation.start(preview, Response.create_all(preview)))

    assert KubernetesV2.start(preview) == Response.read_all(preview)
  end

  test "stop/1", %{preview: preview} do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Namespace.delete(preview, Response.Namespace.delete(preview)))

    assert KubernetesV2.stop(preview) == Response.Namespace.delete(preview)
  end

  test "read/1", %{preview: preview} do
    App.KubernetesV2Mock
    |> expect(:async, Expectation.read_all(preview, Response.read_all(preview)))

    assert KubernetesV2.read(preview) == Response.read_all(preview)
  end

  test "fetch_preview_pod/1", %{preview: preview} do
    App.KubernetesV2Mock
    |> expect(
      :run,
      Expectation.Pod.list(
        preview,
        {:ok,
         %{
           "items" => [
             Response.Pod.docker(preview),
             Response.Pod.preview(preview)
           ]
         }}
      )
    )

    assert KubernetesV2.fetch_preview_pod(preview) == %{
             "metadata" => %{
               "creationTimestamp" => "2016-07-01T11:03:58.970155Z",
               "labels" => %{"app" => "preview"},
               "name" => "podname"
             },
             "status" => %{
               "containerStatuses" => [
                 %{"name" => "preview", "ready" => true, "restartCount" => 4}
               ],
               "initContainerStatuses" => [
                 %{
                   "name" => "fetch-code",
                   "state" => %{"terminated" => %{"reason" => "Completed"}}
                 },
                 %{
                   "name" => "extract-code",
                   "state" => %{"terminated" => %{"reason" => "Completed"}}
                 }
               ],
               "phase" => "Running"
             }
           }
  end

  describe "catchup/1" do
    test "with successful catchup", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:ok,
           %{
             "items" => [
               Response.Pod.docker(preview),
               Response.Pod.preview(preview)
             ]
           }}
        )
      )

      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 200}}
      end)

      assert KubernetesV2.catchup(preview) ==
               {:ok,
                %{
                  readiness: [
                    %{
                      status: :ready,
                      port: %{"name" => "App", "value" => 80}
                    }
                  ],
                  provisioning: %{docker: true},
                  status: %{code_extracted: true, code_fetched: true}
                }}
    end

    test "with unsuccessful catchup", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:error, %{}}
        )
      )

      assert KubernetesV2.catchup(preview) == {:error, %{}}
    end
  end

  test "catchup_with_logs/1", %{preview: preview} do
    log_response = {:ok, "Log line\nAnother log line"}

    App.KubernetesV2Mock
    |> expect(
      :run,
      Expectation.Pod.list(
        preview,
        {:ok,
         %{
           "items" => [
             Response.Pod.docker(preview),
             Response.Pod.preview(preview)
           ]
         }}
      )
    )
    |> expect(:run, Expectation.Pod.log(preview, log_response))

    App.HTTPoisonMock
    |> expect(:request, fn method, request_url ->
      assert method == :get
      assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

      {:ok, %HTTPoison.Response{status_code: 200}}
    end)

    assert KubernetesV2.catchup_with_logs(preview) == %{
             readiness: [
               %{
                 status: :ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ],
             provisioning: %{docker: true},
             status: %{code_extracted: true, code_fetched: true},
             logs: "Log line\nAnother log line"
           }
  end

  test "catchup_boot/1", %{preview: preview} do
    assert KubernetesV2.catchup_boot(preview) == %{
             readiness: [
               %{
                 status: :not_ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ],
             provisioning: %{docker: false},
             status: %{code_extracted: false, code_fetched: false}
           }
  end

  test "unhealthy?", %{preview: preview} do
    App.UtilsMock

    # last_viewed_at startup time
    |> expect(:utc_now, fn ->
      {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
      result
    end)

    # last_viewed_at compared against current
    |> expect(:utc_now, fn ->
      {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
      result
    end)

    # total runtime
    |> expect(:utc_now, fn ->
      {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
      result
    end)

    {:ok, pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])

    App.KubernetesV2Mock
    |> expect(:async, Expectation.read_all(preview, Response.read_all(preview)))
    |> expect(:run, Expectation.Pod.list_preview(preview, Response.Pod.list_preview(preview)))
    |> expect(:run, Expectation.Namespace.get(preview, Response.Namespace.get(preview)))

    assert KubernetesV2.unhealthy?(preview) == false

    GenServer.stop(pid)
  end

  describe "exists?/1" do
    test "when found", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(:async, Expectation.read_all(preview, Response.read_all(preview)))

      assert KubernetesV2.exists?(preview) == true
    end

    test "when not found", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      assert KubernetesV2.exists?(preview) == false
    end
  end

  describe "expired_updated_at?/1" do
    test "when not expired", %{preview: preview} do
      App.UtilsMock

      # last_viewed_at startup time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      # current time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      {:ok, pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])

      assert KubernetesV2.expired_updated_at?(preview) == false

      GenServer.stop(pid)
    end

    test "when expired", %{preview: preview} do
      App.UtilsMock

      # last_viewed_at startup time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      # current time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:08:58.970155Z")
        result
      end)

      {:ok, pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])

      assert KubernetesV2.expired_updated_at?(preview) == true

      GenServer.stop(pid)
    end
  end

  describe "exceeded_total_runtime?/1" do
    test "when not has not exceeded total runtime", %{preview: preview} do
      App.UtilsMock
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.get(preview, Response.Namespace.get(preview)))

      assert KubernetesV2.exceeded_total_runtime?(preview) == false
    end

    test "when has exceeded total runtime", %{preview: preview} do
      App.UtilsMock
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.get(preview, Response.Namespace.get_expired(preview)))

      assert KubernetesV2.exceeded_total_runtime?(preview) == true
    end
  end

  describe "exceeded_restarts?/1" do
    test "when less than allowed restarts", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(:run, Expectation.Pod.list_preview(preview, Response.Pod.list_preview(preview)))

      assert KubernetesV2.exceeded_restarts?(preview) == false
    end

    test "when greater than allowed restarts", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :run,
        Expectation.Pod.list_preview(preview, Response.Pod.list_preview_exceeded(preview))
      )

      assert KubernetesV2.exceeded_restarts?(preview) == true
    end
  end

  describe "readiness/1" do
    test "when found", %{preview: preview} do
      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 200}}
      end)

      assert KubernetesV2.readiness(preview) == [
               %{
                 status: :ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ]
    end

    test "when timeout on connection", %{preview: preview} do
      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:error, %HTTPoison.Error{id: nil, reason: :timeout}}
      end)

      assert KubernetesV2.readiness(preview) == [
               %{
                 status: :not_ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ]
    end

    test "when service not available", %{preview: preview} do
      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 503}}
      end)

      assert KubernetesV2.readiness(preview) == [
               %{
                 status: :not_ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ]
    end

    test "when service not found", %{preview: preview} do
      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 404}}
      end)

      assert KubernetesV2.readiness(preview) == [
               %{
                 status: :not_ready,
                 port: %{"name" => "App", "value" => 80}
               }
             ]
    end
  end

  describe "preview_url/2" do
    test "with insecure protocol" do
      assert KubernetesV2.preview_url("myname", "http://example.com") ==
               "http://myname.example.com"
    end

    test "with secure protocol" do
      assert KubernetesV2.preview_url("othername", "https://example.com") ==
               "https://othername.example.com"
    end

    test "with port" do
      assert KubernetesV2.preview_url("anothername", "https://example.com:4567") ==
               "https://anothername.example.com:4567"
    end
  end
end
