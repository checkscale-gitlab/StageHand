defmodule App.Previews.KubernetesV2.Resources.NamespaceTest do
  alias App.{
    Previews.KubernetesV2.Resources.Namespace
  }

  use App.DataCase, async: true

  @resource %{
    "metadata" => %{
      "annotations" => %{
        "stagehand" => "{\"updated_at\":\"2019-09-15T08:19:52.871524Z\"}"
      },
      "creationTimestamp" => "2019-09-15T08:14:43Z"
    }
  }

  test "updated_at/1" do
    {:ok, datetime, 0} = Namespace.updated_at(@resource)
    assert DateTime.to_iso8601(datetime) == "2019-09-15T08:19:52.871524Z"
  end

  test "created_at/1" do
    {:ok, datetime, 0} = Namespace.created_at(@resource)
    assert DateTime.to_iso8601(datetime) == "2019-09-15T08:14:43Z"
  end
end
