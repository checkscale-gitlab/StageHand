defmodule App.Previews.DynamicSupervisorTest do
  alias App.{Previews.DynamicSupervisor}
  import App.Fixtures
  use App.DataCase

  setup do
    provider = insert(:provider)
    preview = insert(:preview, provider: provider)

    %{preview: preview, provider: provider}
  end

  def starts_child_without_error(preview) do
    assert {:ok, pid} = DynamicSupervisor.start_child(preview)
    assert is_pid(pid)
    assert Process.alive?(pid)
    DynamicSupervisor.terminate_child(preview)
    refute Process.alive?(pid)
  end

  describe "start_child/2" do
    test "starts a single child", %{preview: preview} do
      starts_child_without_error(preview)
    end

    test "starts two children without error", %{preview: preview_1, provider: provider} do
      project = insert(:project, id: "47c18f83-a15b-48a1-ba72-fcc1da97df91")

      preview_2 =
        insert(:preview,
          id: "300f0633-1350-404b-ace8-2d5d825ad789",
          provider: provider,
          project: project
        )

      starts_child_without_error(preview_1)
      starts_child_without_error(preview_2)
    end
  end

  test "terminate_child/2", %{preview: preview} do
    {:ok, pid} = DynamicSupervisor.start_child(preview)
    assert :ok == DynamicSupervisor.terminate_child(preview)
    refute Process.alive?(pid)
  end
end
