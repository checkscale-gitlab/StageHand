defmodule App.PreviewsTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews
  }

  import App.Fixtures
  import ExUnit.CaptureLog
  import Mox
  use App.DataCase, async: false

  setup :set_mox_global
  setup :verify_on_exit!

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)
    insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)
    insert(:provider_connection, project: project, provider: provider, user: user)
    preview = insert(:preview, project: project, provider: provider)

    %{preview: preview}
  end

  test "get/1", %{preview: preview} do
    assert preview.id == Previews.get(preview.id).id
  end

  describe "get_or_create_by/1" do
    test "when does not exist" do
      project = insert(:project, id: "17c18f32-8170-4bf9-81ec-8b3307063032")
      provider = insert(:provider, name: "github")
      repo_slug = "repo_slug"
      sha = "sha"
      username = "username"

      {:ok, preview} =
        Previews.get_or_create_by(%{
          project_id: project.id,
          provider_id: provider.id,
          repo_slug: repo_slug,
          sha: sha,
          username: username,
          version: 1
        })

      assert preview.provider_id == provider.id
      assert preview.project_id == project.id
      assert preview.repo_slug == repo_slug
      assert preview.sha == sha
      assert preview.username == username
    end

    test "when does exist", %{preview: preview} do
      {:ok, returned_preview} =
        Previews.get_or_create_by(
          project_id: preview.project_id,
          provider_id: preview.provider_id
        )

      assert preview.id == returned_preview.id
    end
  end

  describe "launch/1" do
    test "with successful launch", %{preview: preview} do
      App.Mocks.KubernetesV2.start(preview)
      assert {:ok, :success} == Previews.launch(preview)
      Previews.DynamicSupervisor.terminate_child(preview)
    end

    test "with forbidden error in launch", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :async,
        Expectation.read_all(preview, Response.read_all_missing(preview))
      )

      App.KubernetesV2Mock
      |> expect(:async, Expectation.start(preview, Response.create_error_forbidden(preview)))

      assert {:error, :forbidden} ==
               Previews.launch(preview)
    end

    test "with unknown error in launch", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :async,
        Expectation.read_all(preview, Response.read_all_missing(preview))
      )

      App.KubernetesV2Mock
      |> expect(:async, Expectation.start(preview, Response.create_error_unknown(preview)))

      assert {:error, :unknown} == Previews.launch(preview)
    end

    test "when supervisor exists already", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :async,
        Expectation.read_all(preview, Response.read_all_missing(preview))
      )
      |> expect(:async, Expectation.start(preview, Response.create_all(preview)))

      assert {:ok, pid} = Previews.DynamicSupervisor.start_child(preview)
      assert {:error, :already_started} == Previews.launch(preview)
      Previews.DynamicSupervisor.terminate_child(preview)
    end
  end

  test "terminate/1", %{preview: preview} do
    assert {:ok, pid} = Previews.DynamicSupervisor.start_child(preview)
    App.Mocks.KubernetesV2.stop(preview)
    assert Previews.terminate(preview) == :ok
    :timer.sleep(50)
    refute Previews.DynamicSupervisor.Monitor.exists?(preview)
  end

  test "log/4", %{preview: preview} do
    assert capture_log(fn ->
             Previews.log(preview, :info, "prefix", "message")
           end) =~ "#{App.KubernetesV2.kid(preview.id)}: [prefix]: message"
  end
end
