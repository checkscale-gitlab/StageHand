defmodule App.Previews.CommitsTest do
  alias App.{
    Previews.Commits,
    Mocks.Http
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup do
    Mox.verify_on_exit!()
  end

  describe "status/3" do
    test "when bitbucket" do
      provider = insert(:provider, name: "bitbucket")
      project = insert(:project, access_token: "29e41d9a-4ca9-484b-82c1-df01dff3ab40")

      provider_authorisation =
        insert(:provider_authorisation, provider: provider, expires_at: 1_654_724_864)

      insert(:provider_connection,
        provider: provider,
        user: provider_authorisation.user,
        project: project
      )

      preview =
        insert(:preview, %{
          provider: provider,
          project: project,
          repo_slug: "repo_slug",
          sha: "sha",
          username: "username"
        })

      feedback = %{
        "name" => "Custom Title",
        "description" => "All about it"
      }

      App.HTTPoisonMock
      |> expect(
        :request,
        Http.Bitbucket.create_status_build(
          %{
            username: preview.username,
            repo_slug: preview.repo_slug,
            sha: preview.sha,
            feedback: feedback
          },
          preview
        )
      )

      Commits.status(preview, :ready, feedback)
    end

    test "when github" do
      provider = insert(:provider, name: "github")
      project = insert(:project, access_token: "29e41d9a-4ca9-484b-82c1-df01dff3ab40")

      provider_authorisation = insert(:provider_authorisation, provider: provider)

      insert(:provider_connection,
        provider: provider,
        user: provider_authorisation.user,
        project: project
      )

      preview =
        insert(:preview, %{
          provider: provider,
          project: project,
          repo_slug: "repo_slug",
          sha: "sha",
          username: "username"
        })

      feedback = %{
        "name" => "Custom Title",
        "description" => "All about it"
      }

      App.HTTPoisonMock
      |> expect(
        :request,
        Http.Github.create_status(
          %{
            username: preview.username,
            repo_slug: preview.repo_slug,
            sha: preview.sha,
            feedback: feedback
          },
          preview
        )
      )

      Commits.status(preview, :ready, feedback)
    end
  end
end
