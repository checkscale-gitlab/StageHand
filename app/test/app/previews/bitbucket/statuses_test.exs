defmodule App.Previews.Bitbucket.StatusesTest do
  alias App.{
    Previews.Bitbucket.Statuses,
    Mocks.Http
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup :verify_on_exit!

  test "status/1" do
    provider = insert(:provider, name: "bitbucket")
    project = insert(:project, access_token: "29e41d9a-4ca9-484b-82c1-df01dff3ab40")

    provider_authorisation =
      insert(:provider_authorisation, provider: provider, expires_at: 1_654_724_864)

    insert(:provider_connection,
      provider: provider,
      user: provider_authorisation.user,
      project: project
    )

    bitbucket_preview =
      insert(:preview, %{
        provider: provider,
        project: project,
        repo_slug: "repo_slug",
        sha: "sha",
        username: "username"
      })

    feedback = %{
      "key" => "key",
      "name" => "Custom Title",
      "description" => "All about it"
    }

    App.HTTPoisonMock
    |> expect(
      :request,
      Http.Bitbucket.create_status_build(
        %{
          username: bitbucket_preview.username,
          repo_slug: bitbucket_preview.repo_slug,
          sha: bitbucket_preview.sha,
          feedback: feedback
        },
        bitbucket_preview
      )
    )

    Statuses.create(bitbucket_preview, :ready, feedback)
  end
end
