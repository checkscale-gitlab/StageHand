defmodule App.KubernetesV2.Templates.AnnotationTest do
  alias App.{
    KubernetesV2.Templates.Annotation
  }

  use App.DataCase

  test "get/0" do
    assert Annotation.get() == %{
             "stagehand" => "{}"
           }
  end

  test "parse/1" do
    resource = %{
      "metadata" => %{
        "annotations" => %{
          "stagehand" => "{}"
        }
      }
    }

    assert Annotation.parse(resource) == %{}
  end
end
