defmodule App.KubernetesV2.MonitorTest do
  alias App.{
    KubernetesV2.Monitor,
    Mocks.KubernetesV2.Expectation,
    Previews
  }

  import App.Fixtures
  import ExUnit.CaptureLog
  import Mox

  use App.DataCase, async: false

  @requeue_time Application.get_env(:stagehand, :kubernetes_resource_poll_interval)

  setup do
    Mox.set_mox_global()
    Mox.verify_on_exit!()

    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)
    insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)
    insert(:provider_connection, project: project, provider: provider, user: user)
    preview = insert(:preview, project: project, provider: provider)

    result =
      {:ok,
       %{
         "items" => [
           %{
             "metadata" => %{
               "labels" => %{
                 "preview_id" => preview.id
               }
             }
           }
         ]
       }}

    result_timeout = {:error, %HTTPoison.Error{id: nil, reason: :timeout}}

    result_empty =
      {:ok,
       %{
         "items" => []
       }}

    %{
      preview: preview,
      result: result,
      result_empty: result_empty,
      result_timeout: result_timeout
    }
  end

  test "start_link/0", %{result: result, preview: preview} do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Namespace.list(result))

    App.Mocks.KubernetesV2.start(preview)

    assert {:ok, pid} = Monitor.start_link()
    assert is_pid(pid)
    GenServer.stop(pid)
    refute Process.alive?(pid)

    Previews.DynamicSupervisor.terminate_child(preview)
  end

  # Executes in the context of this process
  test "init/1", %{result: result, preview: preview} do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Namespace.list(result))

    App.Mocks.KubernetesV2.start(preview)

    state = %{}
    assert Monitor.init(state) == {:ok, state}

    :timer.sleep(@requeue_time)
    assert_received :work

    Previews.DynamicSupervisor.terminate_child(preview)
  end

  test "handle_info/2", %{result: result, preview: preview} do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Namespace.list(result))

    App.Mocks.KubernetesV2.start(preview)

    state = %{}
    assert Monitor.handle_info(:work, state) == {:noreply, state}

    :timer.sleep(@requeue_time)
    assert_received :work

    Previews.DynamicSupervisor.terminate_child(preview)
  end

  describe "work/0" do
    test "with no previews running", %{result_empty: result_empty} do
      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.list(result_empty))

      logs =
        capture_log(fn ->
          Monitor.work()
        end)

      assert logs =~ "Scanning for orphaned previews"
      assert logs =~ "Orphaned preview scan completed"
    end

    test "with orphaned preview running", %{result: result, preview: preview} do
      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.list(result))

      App.Mocks.KubernetesV2.start(preview)

      logs =
        capture_log(fn ->
          Monitor.work()
        end)

      assert logs =~ "Scanning for orphaned previews"
      assert logs =~ "Checking preview: #{preview.id}"
      assert logs =~ "Orphaned preview found, relaunching..."
      assert logs =~ "Finished preview check for: #{preview.id}"
      assert logs =~ "Orphaned preview scan completed"

      assert Previews.DynamicSupervisor.Monitor.exists?(preview)

      Previews.DynamicSupervisor.terminate_child(preview)
    end

    test "with incomplete preview running", %{result: result, preview: preview} do
      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.list(result))

      {:ok, _pid} = Previews.DynamicSupervisor.start_child(preview)

      App.Mocks.KubernetesV2.stop(preview)

      logs =
        capture_log(fn ->
          Monitor.work()
        end)

      assert logs =~ "Scanning for orphaned previews"
      assert logs =~ "Checking preview: #{preview.id}"
      assert logs =~ "Incomplete preview found, terminating preview"
      assert logs =~ "Finished preview check for: #{preview.id}"
      assert logs =~ "Orphaned preview scan completed"
    end

    test "with timeout in request", %{result_timeout: result, preview: preview} do
      App.KubernetesV2Mock
      |> expect(:run, Expectation.Namespace.list(result))

      {:ok, _pid} = Previews.DynamicSupervisor.start_child(preview)

      logs =
        capture_log(fn ->
          Monitor.work()
        end)

      assert logs =~ "Scanning for orphaned previews"
      assert logs =~ "Error in monitor scan: %HTTPoison.Error{id: nil, reason: :timeout}"
      assert logs =~ "Orphaned preview scan completed"

      Previews.DynamicSupervisor.terminate_child(preview)
    end
  end

  test "queue/0" do
    ref = Monitor.queue()
    assert is_reference(ref)

    :timer.sleep(@requeue_time)
    assert_received :work
  end

  test "options/0" do
    assert Monitor.options() == [params: %{"labelSelector" => "stagehand=true"}]
  end
end
