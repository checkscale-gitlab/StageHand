defmodule App.KubernetesV2.TemplatesTest do
  alias App.{
    KubernetesV2.Templates
  }

  use App.DataCase, async: true

  test "label/0" do
    assert Templates.label() == %{"stagehand" => "true"}
  end
end
