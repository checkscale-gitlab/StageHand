defmodule App.KubernetesV2Test do
  alias App.{
    KubernetesV2
  }

  import Mox
  use App.DataCase, async: true

  @kubernetes_v2 Application.get_env(:stagehand, :kubernetes_v2)

  setup do
    Mox.verify_on_exit!()
  end

  test "async/1" do
    App.KubernetesV2Mock
    |> expect(:async, fn resource ->
      assert resource == [%{}]
      {:ok, :result}
    end)

    assert {:ok, :result} == @kubernetes_v2.async([%{}])
  end

  test "run/1" do
    App.KubernetesV2Mock
    |> expect(:run, fn resource ->
      assert resource == %{}
      {:ok, :result}
    end)

    assert {:ok, :result} == @kubernetes_v2.run(%{})
  end

  test "kid/1" do
    assert KubernetesV2.kid("1234") == "id03ac674216f3e"
  end
end
