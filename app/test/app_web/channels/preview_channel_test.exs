defmodule AppWeb.PreviewChannelTest do
  alias AppWeb.{PreviewChannel}

  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews
  }

  import App.Fixtures
  import Mox

  use AppWeb.ChannelCase, async: false

  setup :set_mox_global
  setup :verify_on_exit!

  setup do
    preview = insert(:preview)
    socket = Phoenix.Socket.assign(socket(AppWeb.PublicSocket), :preview, preview)

    {:ok, socket: socket, preview: preview}
  end

  describe "join/3" do
    test "handles preview record that cannot be found or accessed", %{
      socket: socket
    } do
      assert {:error, "Preview not found"} ==
               subscribe_and_join(
                 socket,
                 PreviewChannel,
                 "preview:a3a74f13-380b-49df-aaad-368103534090"
               )
    end

    test "handles preview that exists but not running in kubernetes", %{
      socket: socket,
      preview: preview
    } do
      App.KubernetesV2Mock
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      assert {:error, "Preview not running"} ==
               subscribe_and_join(
                 socket,
                 PreviewChannel,
                 "preview:" <> preview.id
               )
    end
  end

  describe "handle_in/3" do
    test "can send heartbeat", %{
      socket: socket,
      preview: preview
    } do
      socket = Phoenix.Socket.assign(socket, :preview, preview)

      App.UtilsMock

      # startup time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
        result
      end)

      # sent time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
        result
      end)

      {:ok, pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])

      assert {:noreply, returned_socket} = PreviewChannel.handle_in("heartbeat", nil, socket)
      assert returned_socket == socket

      GenServer.stop(pid)
    end
  end

  test "channel_name/1", %{
    preview: preview
  } do
    assert "preview:" <> preview.id ==
             PreviewChannel.channel_name(preview)
  end

  describe "send_message/3" do
    test "log message", %{
      preview: preview
    } do
      data = "data"
      AppWeb.Endpoint.subscribe("preview:" <> preview.id)
      PreviewChannel.send_message(preview, :log, data)
      assert_broadcast("log", %{data: data})
    end

    test "preview message", %{
      preview: preview
    } do
      data = "data"
      AppWeb.Endpoint.subscribe("preview:" <> preview.id)
      PreviewChannel.send_message(preview, :preview, data)
      assert_broadcast("preview", %{data: data})
    end
  end
end
