defmodule App.Mocks.Http.Bitbucket do
  use ExUnit.Case, async: true

  def create_status_build(
        %{username: username, repo_slug: repo_slug, sha: sha, feedback: feedback},
        preview \\ nil
      ) do
    fn method, url, body, _headers ->
      assert method == :post

      assert url ==
               "https://api.bitbucket.org/2.0/repositories/#{username}/#{repo_slug}/commit/#{sha}/statuses/build"

      body = Poison.decode!(body)
      assert body["description"] == feedback["description"]
      assert body["key"] == feedback["key"]
      assert body["name"] == feedback["name"]
      assert body["state"] == "SUCCESSFUL"

      if preview do
        assert body["url"] =~ "#{AppWeb.Endpoint.url()}/preview?id=#{preview.id}"
      else
        assert body["url"] =~ "#{AppWeb.Endpoint.url()}/preview?id="
      end

      {:ok, %HTTPoison.Response{}}
    end
  end
end
