defmodule App.Mocks.KubernetesV2.Expectation do
  alias App.{
    Mocks.KubernetesV2.Operation
  }

  use ExUnit.Case, async: true

  def read_all(preview, response) do
    fn operation ->
      services =
        preview.configuration["ports"]
        |> Enum.map(fn port ->
          Operation.Service.get(preview, port)
        end)

      assert operation ==
               [Operation.Namespace.get(preview)] ++
                 services ++
                 [Operation.ServiceDocker.get(preview)] ++
                 [Operation.Deployment.get(preview), Operation.DeploymentDocker.get(preview)]

      response
    end
  end

  def start(preview, response) do
    fn operation ->
      services =
        preview.configuration["ports"]
        |> Enum.map(fn port ->
          Operation.Service.create(preview, port)
        end)

      assert operation ==
               [Operation.Namespace.create(preview)] ++
                 services ++
                 [Operation.ServiceDocker.create(preview)] ++
                 [
                   Operation.Deployment.create(preview),
                   Operation.DeploymentDocker.create(preview)
                 ]

      response
    end
  end
end
