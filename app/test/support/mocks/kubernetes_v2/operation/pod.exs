defmodule App.Mocks.KubernetesV2.Operation.Pod do
  def list(_preview) do
    %K8s.Operation{
      api_version: "v1",
      data: nil,
      method: :get,
      name: "pods",
      path_params: [namespace: "id9f12b94792201"],
      verb: :list
    }
  end

  def list_docker(_preview) do
    %K8s.Operation{
      api_version: "v1",
      data: nil,
      method: :get,
      name: "pods",
      path_params: [namespace: "id9f12b94792201"],
      verb: :list,
      label_selector: %K8s.Selector{match_expressions: [], match_labels: %{"app" => "docker"}}
    }
  end

  def list_preview(_preview) do
    %K8s.Operation{
      api_version: "v1",
      data: nil,
      method: :get,
      name: "pods",
      path_params: [namespace: "id9f12b94792201"],
      verb: :list,
      label_selector: %K8s.Selector{match_expressions: [], match_labels: %{"app" => "preview"}}
    }
  end

  def log(_preview) do
    %K8s.Operation{
      api_version: "v1",
      data: nil,
      label_selector: nil,
      method: :get,
      name: "pods/log",
      path_params: [namespace: "id9f12b94792201", name: "podname"],
      verb: :get
    }
  end
end
