// @flow

import * as React from "react";
import ReactDOM from "react-dom";

const renderApp = (Component) => {
  const appElement = document.getElementById("app");

  if (!appElement) {
    throw new Error("Mount element not found");
  }

  ReactDOM.render(Component, appElement);
};

export default renderApp;
