import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

const handleChange = () => {
  store.requestingLogs = !store.requestingLogs
}

export const PreviewShowLogs = props => {
  return useObserver(() => {
    return (
      <label className={styles.root}>
        <input
          checked={store.showingLogs}
          onChange={handleChange}
          disabled={store.state !== 'previewing'}
          type="checkbox"
        />{' '}
        Show logs
      </label>
    )
  })
}

export default PreviewShowLogs
