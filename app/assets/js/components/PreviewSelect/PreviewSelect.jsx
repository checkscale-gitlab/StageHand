import * as React from 'react'
import { map } from 'lodash/fp'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import { PreviewSelectOption } from '@/components/PreviewSelectOption/PreviewSelectOption.jsx'

import styles from './styles.css'

export const PreviewSelect = () => {
  return useObserver(() => {
    const renderedTabs = map(port => (
      <PreviewSelectOption key={port.url} port={port} />
    ))(store.ports.toJS())

    return <ul className={styles.root}>{renderedTabs}</ul>
  })
}

export default PreviewSelect
