import * as React from 'react'
import { useObserver } from 'mobx-react-lite'
import classNames from 'classnames'
import { map } from 'lodash/fp'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

export const PreviewShow = () => {
  return useObserver(() => {
    const renderedPreviews = map(preview => {
      const classes = classNames({
        [styles.root]: true,
        [styles.hidden]: store.selectedPort.url !== preview.url
      })

      return <iframe className={classes} key={preview.url} src={preview.url} />
    })(store.ports)

    return <>{renderedPreviews}</>
  })
}

export default PreviewShow
