defmodule AppWeb.ApplicationController do
  use AppWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html", conn: conn)
  end

  def healthcheck(conn, _params) do
    send_resp(conn, 200, "")
  end
end
