defmodule AppWeb.PreviewChannel do
  alias App.{
    Previews
  }

  use Phoenix.Channel

  @utils Application.get_env(:stagehand, :utils)

  @doc """
  Handles a user joining a preview channel.
  TODO: Test running here as well
  """
  def join("preview:" <> id, _params, socket) do
    preview = Previews.get(id)

    cond do
      preview == nil ->
        {:error, "Preview not found"}

      Previews.KubernetesV2.exists?(preview) == false ->
        {:error, "Preview not running"}

      true ->
        socket = assign(socket, :preview, preview)
        {:ok, socket}
    end
  end

  @doc """
  Sent by the client to indicate its still alive, viewing the preview
  TODO: Convert to built in presence module
  """
  def handle_in("heartbeat", nil, socket) do
    preview = socket.assigns[:preview]
    pid = Previews.DynamicSupervisor.Monitor.State.whereis(preview)
    GenServer.cast(pid, {:last_viewed_at, @utils.utc_now()})
    {:noreply, socket}
  end

  @doc """
  The channel name for previews
  """
  def channel_name(preview) do
    "preview:" <> preview.id
  end

  @doc """
  Sends a log entry message
  """
  def send_message(preview, :log, data) do
    send_message(preview, "log", data)
  end

  @doc """
  Sends a status message (status of the preview)
  """
  def send_message(preview, :preview, data) do
    send_message(preview, "preview", data)
  end

  def send_message(preview, topic, data) do
    channel_name(preview)
    |> AppWeb.Endpoint.broadcast(topic, %{data: data})
  end
end
