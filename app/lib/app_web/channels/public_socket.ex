defmodule AppWeb.PublicSocket do
  use Phoenix.Socket

  channel("preview:*", AppWeb.PreviewChannel)

  transport(:websocket, Phoenix.Transports.WebSocket)

  def connect(%{"token" => token}, socket) do
    salt = Application.get_env(:stagehand, AppWeb.Endpoint)[:secret_key_base]

    case Phoenix.Token.verify(socket, salt, token, max_age: 86400) do
      {:ok, _} ->
        {:ok, socket}

      {:error, _} ->
        :error
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     AppWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
end
