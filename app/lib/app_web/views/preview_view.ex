defmodule AppWeb.PreviewView do
  use AppWeb, :view

  def render("styles.css", _assigns) do
    ~E(
      <style type="text/css">
        body {
          background: #222;
          color: #fff;
          margin: 0;
        }
      </style>
    )
  end
end
