defmodule AppWeb.Api.PreviewView do
  use AppWeb, :view

  def render("show.json", %{preview: preview}) do
    configuration = Map.drop(preview.configuration, ["access_token"])

    %{
      configuration: configuration,
      id: preview.id,
      preview_url: AppWeb.Router.Helpers.preview_path(AppWeb.Endpoint, :show, %{id: preview.id})
    }
  end
end
