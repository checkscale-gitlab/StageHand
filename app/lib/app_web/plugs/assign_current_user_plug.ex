defmodule AppWeb.AssignCurrentUserPlug do
  import Plug.Conn

  @behaviour Plug

  def init(opts), do: opts

  def call(conn, _) do
    current_user = Guardian.Plug.current_resource(conn)

    if current_user do
      conn
      |> assign(:current_user, current_user)
    else
      conn
      |> send_resp(401, "{\"error\":\"unauthorized\"}")
      |> halt
    end
  end
end
