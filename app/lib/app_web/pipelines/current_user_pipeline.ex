defmodule AppWeb.CurrentUserPipeline do
  use Guardian.Plug.Pipeline, otp_app: :stagehand, module: App.Guardian
  plug(:fetch_session)
  plug(Guardian.Plug.VerifySession)
  plug(Guardian.Plug.VerifyHeader)
  plug(Guardian.Plug.LoadResource, allow_blank: true)
  plug(AppWeb.AssignCurrentUserPlug)
end
