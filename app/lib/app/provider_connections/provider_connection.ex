defmodule App.ProviderConnections.ProviderConnection do
  alias App.{
    Projects,
    Providers,
    Users
  }

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "provider_connections" do
    belongs_to(:provider, Providers.Provider)
    belongs_to(:project, Projects.Project)
    belongs_to(:user, Users.User)
    timestamps()
  end

  @doc false
  def changeset(model, attrs \\ %{}) do
    model
    |> cast(attrs, [:user_id, :project_id, :provider_id])
    |> validate_required([:user_id, :project_id, :provider_id])
  end
end
