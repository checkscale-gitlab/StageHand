defmodule App.Projects do
  alias App.{Projects, Repo}

  @model Projects.Project
  @struct %Projects.Project{}

  def create(attrs) do
    @struct
    |> @model.changeset(attrs)
    |> Repo.insert()
  end

  def get(id) do
    Repo.get(@model, id)
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def all do
    Repo.all(@model)
  end

  @doc """
  Returns the total number of projects in the system
  """
  def total do
    Repo.aggregate(Projects.Project, :count, :id)
  end
end
