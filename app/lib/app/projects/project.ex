defmodule App.Projects.Project do
  alias App.{ProviderConnections}
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "projects" do
    field(:access_token, :binary_id)
    field(:name, :string)
    has_many(:provider_connections, ProviderConnections.ProviderConnection)
    timestamps()
  end

  @doc false
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
