defmodule App.Previews.ProviderAuthorisations do
  alias App.{
    Previews,
    ProviderAuthorisations,
    ProviderConnections,
    Repo,
    Users
  }

  import Ecto.Query, only: [from: 2]

  @doc """
  Get the provier authorisation for a given preview
  """
  @spec get(%Previews.Preview{}) :: %ProviderAuthorisations.ProviderAuthorisation{}
  def get(%{provider_id: provider_id, project_id: project_id}) do
    from(provider_authorisation in ProviderAuthorisations.ProviderAuthorisation,
      distinct: true,
      join: user in Users.User,
      on: user.id == provider_authorisation.user_id,
      join: provider_connection in ProviderConnections.ProviderConnection,
      on: provider_connection.user_id == user.id,
      where: provider_authorisation.provider_id == ^provider_id,
      where: provider_connection.project_id == ^project_id
    )
    |> Repo.one()
  end
end
