defmodule App.Previews.KubernetesV2.Templates.Pod do
  @moduledoc """
  Defines a namespace that previews related to the project will run in.
  """

  alias App.{
    Previews
  }

  @doc """
  Get all pods in namespace
  """
  @spec all(%Previews.Preview{}) :: K8s.Operation.t()
  def all(preview) do
    K8s.Client.list("v1", "pods", namespace: Previews.KubernetesV2.Templates.namespace(preview))
  end

  @doc """
  Get in namespace with an app name
  """
  @spec all(%Previews.Preview{}, String.t()) :: K8s.Operation.t()
  def all(preview, name) do
    all(preview)
    |> K8s.Selector.label({"app", name})
  end

  @doc """
  Get logs for selected container
  """
  @spec logs({Previews.Preview}, String.t()) :: K8s.Operation.t()
  def logs({preview}, name) do
    K8s.Client.get("v1", "pods/log",
      namespace: Previews.KubernetesV2.Templates.namespace(preview),
      name: name
    )
  end
end
