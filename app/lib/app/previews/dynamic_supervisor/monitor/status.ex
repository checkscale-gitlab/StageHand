defmodule App.Previews.DynamicSupervisor.Monitor.Status do
  alias App.{
    Previews,
    Previews.DynamicSupervisor.Monitor
  }

  use GenServer
  use Previews.DynamicSupervisor.Monitor.Base, name: :monitor_status

  # seconds
  @tick_rate 2

  def init([preview]) do
    state = %{
      preview: preview
    }

    schedule_work()

    {:ok, state}
  end

  @doc """
  Executed periodically, this function determines if the preview is responding to traffic
  """
  def handle_info(:work, %{preview: preview} = state) do
    log(preview, "Running check now")

    catchup = Previews.KubernetesV2.catchup(preview)

    case catchup do
      {:ok, catchup} -> handle_catchup(preview, catchup)
      {:error, _} -> nil
    end

    schedule_work()

    {:noreply, state}
  end

  defp handle_catchup(preview, catchup) do
    status = catchup[:status]

    if Monitor.Logs.exists?(preview) == false && status[:code_fetched] == true &&
         status[:code_extracted] == true do
      preview_ready? =
        Previews.KubernetesV2.fetch_preview_pod(preview)
        |> Previews.KubernetesV2.Resources.Pod.ready("preview")

      if preview_ready? == true do
        Monitor.start_module(Monitor.Logs, preview)
      end
    end

    AppWeb.PreviewChannel.send_message(
      preview,
      :preview,
      catchup
    )
  end

  defp schedule_work() do
    Process.send_after(self(), :work, tick_length())
  end

  defp tick_length do
    @tick_rate * 1000
  end
end
