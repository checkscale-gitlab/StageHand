defmodule App.ProviderAuthorisations do
  alias App.{Repo, ProviderAuthorisations}

  @model ProviderAuthorisations.ProviderAuthorisation
  @struct %ProviderAuthorisations.ProviderAuthorisation{}

  def build(attrs) do
    @struct
    |> @model.changeset(attrs)
  end

  def create(attrs) do
    build(attrs)
    |> Repo.insert()
  end

  def update(%ProviderAuthorisations.ProviderAuthorisation{} = provider_authorisation, attrs) do
    provider_authorisation
    |> @model.changeset(attrs)
    |> Repo.update()
  end

  def get(id) do
    Repo.get(@model, id)
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def get_or_create_by(attrs) do
    case get_by(attrs) do
      nil ->
        create(attrs)

      modal ->
        {:ok, modal}
    end
  end

  @doc """
  Auths that don't expire can never be expired
  """
  @spec expired?(%ProviderAuthorisations.ProviderAuthorisation{}) :: boolean()
  def expired?(%{expires: expires}) when expires == false do
    false
  end

  @doc """
  Check provider auth expiration by comparing current time against expires_at less 30 seconds for a buffer.
  """
  @spec expired?(%ProviderAuthorisations.ProviderAuthorisation{}) :: boolean()
  def expired?(%{expires_at: expires_at}) do
    current =
      DateTime.utc_now()
      |> DateTime.to_unix()

    current >= expires_at - 30
  end
end
