defmodule App.ProviderAuthorisations.Bitbucket.Token do
  alias App.{ProviderAuthorisations}

  @http_client Application.get_env(:stagehand, :http_client)

  def renew_if_expired(provider_authorisation) do
    if ProviderAuthorisations.expired?(provider_authorisation) do
      renew(provider_authorisation)
    else
      {:ok, provider_authorisation}
    end
  end

  @doc """
  Renews the token for this provider authorisation.
  """
  def renew(provider_authorisation) do
    config = Application.get_env(:ueberauth, Ueberauth.Strategy.Bitbucket.OAuth)

    {:ok, response} =
      @http_client.post(
        "https://bitbucket.org/site/oauth2/access_token",
        {
          :form,
          [
            {"grant_type", "refresh_token"},
            {"refresh_token", provider_authorisation.refresh_token}
          ]
        },
        [
          {"Content-Type", "application/x-www-form-urlencoded"}
        ],
        hackney: [
          basic_auth: {
            config[:client_id],
            config[:client_secret]
          }
        ]
      )

    decoded_body = Poison.decode!(response.body)
    current_time = DateTime.utc_now() |> DateTime.to_unix()

    ProviderAuthorisations.update(provider_authorisation, %{
      token: decoded_body["access_token"],
      expires_at: current_time + decoded_body["expires_in"]
    })
  end
end
