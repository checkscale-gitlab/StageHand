defmodule App.ProviderAuthorisations.Bitbucket do
  alias App.{ProviderAuthorisations}

  @host "https://api.bitbucket.org"
  @http_client Application.get_env(:stagehand, :http_client)

  def request(provider_authorisation, method, path, body \\ "", headers \\ []) do
    {:ok, provider_authorisation} =
      ProviderAuthorisations.Bitbucket.Token.renew_if_expired(provider_authorisation)

    @http_client.request(
      method,
      Path.join(@host, path),
      Poison.encode!(body),
      Keyword.merge(default_headers(provider_authorisation), headers)
    )
  end

  def default_headers(%{token: token}) do
    [
      Authorization: "Bearer #{token}",
      "Content-Type": "application/json"
    ]
  end
end
