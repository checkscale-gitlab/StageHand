defmodule App.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(App.Repo, []),
      supervisor(AppWeb.Endpoint, []),
      supervisor(App.Previews.DynamicSupervisor, []),
      supervisor(Registry, [:unique, :previews])
    ]

    children =
      children ++
        if Application.get_env(:stagehand, :run_monitor) do
          [
            worker(App.KubernetesV2.Monitor, [])
          ]
        else
          []
        end

    opts = [strategy: :one_for_one, name: App.Supervisor]

    {:ok, _} = Logger.add_backend(Sentry.LoggerBackend)
    Logger.configure_backend(Sentry.LoggerBackend, include_logger_metadata: true)

    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    AppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
